# -*- coding: utf-8 -*-
import requests
import cookielib
import os
import re
import logging
from lxml.html import fromstring as html_fromstring, tostring as html_tostring


cj = cookielib.MozillaCookieJar('legendas.lwp')


def __connect__(url="http://legendas.tv/index.php", data=None):

    logging.debug("Connecting on URL: %s  - using data: %s" % (url, data))
    session = requests.session(cookies=cj)
    return session.post(url, data=data)


def lxml_parser(page, ignore=None):
    parse = html_fromstring(page)
    subtitles = []

    span_results = parse.cssselect('td#conteudodest span table')

    for span in span_results:
        subname = span.cssselect('.mais b')[0]
        if ignore is None or len(filter(lambda x: subname.text.lower().__contains__(x.lower()), ignore)) == 0:
            download_id = re.search('[a-z0-9]{32}', html_tostring(span)).group(0)
            release = span.cssselect('.brls')[0]

            subtitles.append({
                'subname': subname.text,
                'release': release.text,
                'download_id': download_id
                })

    return subtitles


def login(username, password):
    logging.debug("Testing if the cookie file exists")
    if os.path.isfile("legendas.lwp"):
        cj.load(ignore_discard=True)

    logging.debug("Testing if user is logged in")

    if __connect__(data={'opcao': 'alterarperfil'}).text.__contains__("precisa estar logado para acessar"):
        print("Efetuando login no site LegendasTV")
        __connect__(url="http://legendas.tv/login_verificar.php",
                       data={'txtLogin': username, 'txtSenha': password})

        logging.debug("Saving user session")
        cj.save(ignore_discard=True)


def download(subtitle_id, release):
    logging.debug("Downloading release: %s / id: %s" % (release, subtitle_id))

    s = __connect__('http://legendas.tv/info.php',
                                             data={'d': subtitle_id, 'c': '1'})

    tmp_release = release.replace('/', '-')

    if s.headers['content-type'].__contains__('rar'):
        tmp_release += ".rar"
    else:
        tmp_release += ".zip"

    logging.debug("Saving release: %s" % tmp_release)
    open(tmp_release, 'wb').write(s.content)


def search(subtitle, type=1, language=1, ignore=None):
    logging.debug("Searching for: %s" % subtitle)
    r = __connect__(data={'opcao': 'buscarlegenda',
                          'txtLegenda': subtitle,
                          'selTipo': type,
                          'int_idioma': language}).content

    if not r.__contains__("Nenhuma legenda foi encontrada"):
        logging.debug("Found! Parsing page")
        return lxml_parser(r, ignore)


if __name__ == "__main__":

    def readconf():
        import os

        PATH = (lambda *x: reduce(os.path.join, x))
        CONFIG_DIR = PATH((os.getenv('HOME') or os.getenv('USERPROFILE')), ".config", "legendastv")

        if not os.path.exists(CONFIG_DIR):
            os.makedirs(CONFIG_DIR)

        CONFIG = PATH(CONFIG_DIR, 'legendas.cfg')

        if not os.path.exists(CONFIG):
            username = raw_input('Digite seu login no legendasTV: ')
            password = raw_input('Digite sua senha no legendasTV: ')

            open(CONFIG, 'w').write("username=%s\npassword=%s\nignore=" % (username, password))

        f = open(CONFIG)
        conf = {}
        ignore = {}
        for line in f:
            k, v = line.replace('\n', '').split('=')
            if "ignore" == k and v.__contains__(","):
                ignore = v.split(",")
            else:
                conf[k] = v

        return dict(config=conf, ignored=ignore)

    def main():
        import sys

        config = readconf()
        logging.basicConfig(format='%(asctime)s [ %(levelname)s ] >> %(message)s',
                                                        level=logging.DEBUG)
        busca = None
        if len(sys.argv) > 1:
            if sys.argv[1].__contains__('.avi'):
                busca = sys.argv[1].split('.avi')[0]
            else:
                busca = ' '.join(sys.argv[1:])

            print 'Buscando legendas para %s' % busca

        if busca is None:
            busca = raw_input('Digite o nome da legenda que sera buscada: ')

        login(config['config']['username'], config['config']['password'])
        results = search(busca, ignore=config['ignored'])

        if results is None or len(results) == 0:
            print "Nenhuma legenda encontrada"
            sys.exit(0)

        elif len(results) == 1:
            print "1 Legenda encontrada! Efetuando Download"
            download(results[0]['download_id'], results[0]['release'])

        else:
            for index, item in enumerate(results):
                print('Opcao [ %d ] %s' % (index, item['subname']))
                print('Release: %s' % item['release'])
                print('\n')

            id = raw_input('Digite o numero da legenda a ser baixada: ')
            if id == '' or int(id) not in range(len(results)):
                sys.exit(0)

            download(results[int(id)]['download_id'], results[int(id)]['release'])

    main()
